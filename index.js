require('dotenv').config()

const express = require('express')

const jwt = require('jsonwebtoken');

const app = express()

const db = require('./db')

const kue = require('kue')

const demoManager = require('./demoManager')

const cache = require('./cache')

kue.app.listen(8002)

kue.createQueue({
  redis: {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST
    // auth: 'password',
    // db: 3, // if provided select a non-default redis db
    // options: {
    //   // see https://github.com/mranney/node_redis#rediscreateclient
    // }
  }
})

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
});

// const Bot = require('./bot')
//
// Bot.connectLoginAndLaunch(() => {
//   console.log('csgolaunched...');
  app.listen(process.env.PORT, () => {
    console.log('App listening at port %s', process.env.PORT)
  })
// })

app.get('/stats', (req, res) => {
  // team 3 is CT
  // team 2 is T
  cache.getHeatmapData('TEMP', 'pos', req.query.team, req.query.half).then(data => {
    // console.log(`data -> ${JSON.parse(data)}`);
    [].concat.apply([], data)
    res.send(data)
  }).catch(err => {
    res.status(500)
  })
  // res.send(cache.getHeatmapData('TEMP', 'pos', '3', 'first'))
})

app.get('/match', (req, res) => {
  // use a middleware?
  const token = req.headers['x-access-token'];
  if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });

  return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decoded) => {
    if (err) {
      res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });
    } else {
      const matchCode = req.query.matchcode || 'CSGO-UvUEM-MG7Vs-zbfNZ-uT5Jt-8MEKO'
      Bot.findOrRequestMatch(matchCode, (err, url) => {
        res.send(url)
      })
    }
  })
})

app.get('/parse', (req, res) => {
  demoManager.parseFromFile('./replays/match730_003258100594472124446_0369364199_900.dem', (err, buffer) => {
    console.log(`buffer length -> ${buffer.length}`);
    res.status(200).send('done')
  })
})

app.get('/live', (req, res) => {
  // charminar = 42302141
  // fly = 25010566
  // magnet = 174611035
  // phillipine = 836883349
  // another = 348096865
  // and another = 250903863
  const steamID = parseInt(req.query.steamid)
  Bot.requestLiveMatch(steamID, (err, match) => {
    if (err) {
      res.send(err)
    } else {
      const matchId = Bot.getMatchId(match)
      db.createStatsRequest(matchId, { STEAM_ID: '25010566' }, () => {
        console.log('created stats request, starting poller...');
        Bot.enqueueMatchPollerJob(matchId, steamID)
      })
      res.send(match)
    }
  })
})
