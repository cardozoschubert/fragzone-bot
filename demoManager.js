const request = require('request');
const progress = require('request-progress');
const parser = require('./parser')
const fs = require('fs');
const bz2 = require('unbzip2-stream');

// var zlib = require('zlib');
// const s3 = new AWS.S3({
//   accessKeyId: process.env.AWS_ACCESS_KEY_ID,
//   secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
//   region: process.env.AWS_REGION,
// })

// uploadStream = (path, callback) => {
//   const pass = new stream.PassThrough()
//   const params = {
//     Bucket: 'csgodemo',
//     Key: path,
//     Body: pass
//   }
//   s3.upload(params, (err, data) => {
//     callback(err, data)
//     // console.log(err, data);
//   })
//   return pass;
// }

const parseFromFile = file => {
  fs.readFile(file, (err, buffer) => {
    parser.parse('TEMP', buffer)
  })
}

const parseFromUrl = (matchId, url, updateProgress, callback) => {
  const outStream = progress(request(url), {
    // throttle params
  }).on('progress', state => {
    updateProgress((state.percent * 100).toFixed(0))
    console.log(`progress -> ${(state.percent * 100).toFixed(2)}%`)
  }).on('error', err => {
    // Do something with err
  }).on('end', () => {
    console.log('ended stream')
  }).pipe(bz2())

  const bufs = [];
  outStream.on('data', d => {
    bufs.push(d)
    // console.log(`data`);
  })
  outStream.on('end', () => {
    process.nextTick(() => {
      console.log('end')
      const buf = Buffer.concat(bufs);
      parser.parse(matchId, buf)
      callback(null, buf)
    })
  })
}

// parseFromS3 = s3params => {
//   const {Bucket, Key} = s3params
//   console.log(`gettin from s3 ${JSON.stringify({Bucket, Key})}`);
//   s3.getObject({Bucket, Key}, (err, data) => {
//     console.log(`getObject...`);
//     if(err) {
//       console.log(`errrr`);
//     }
//     else {
//       console.log(`zlib`);
//       zlib.gunzip(data.Body, function (err, result) {
//         if (err) {
//           console.log(err);
//         } else {
//           parseBuffer(result)
//         }
//       })
//     }
//   })
// }

module.exports = { parseFromUrl, parseFromFile }
