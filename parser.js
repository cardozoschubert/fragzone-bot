const demofile = require('demofile')

const cache = require('./cache')

const db = require('./db')

const csgoMaps = {
  map_details: {
    de_cache: {
      posX: '-2000',
      posY: '3250',
      scale: '5.5',
    },
    de_cbble: {
      posX: '-3840', // upper left world x coordinate
      posY: '3072', // upper left world y coordinate
      scale: '6',
    },
    de_inferno: {
      posX: '-2087', // upper left world x coordinate
      posY: '3072', // upper left world y coordinate
      scale: '4.9',
    },
    de_mirage: {
      posX: '-3230',
      posY: '1713',
      scale: '5.00',
    },
    de_nuke: {
      posX: '-3453',
      posY: '2887',
      scale: '7.00',
    },
    de_overpass: {
      posX: '-4831',
      posY: '1781',
      scale: '5.2',
    },
    de_train: {
      posX: '-2477',
      posY: '2392',
      scale: '4.7',
    },
  },
};

const translateCoordinates = (map, xGame, yGame) => {
  const { posX, posY } = csgoMaps.map_details[map]
  const scaleFactor = csgoMaps.map_details[map].scale;

  const xPrime = (xGame - posX) / scaleFactor;
  const yPrime = (posY - yGame) / scaleFactor;

  return { x: xPrime, y: yPrime }
}

const parse = (matchId, buffer) => {
  // var fs = require('fs')
  //
  // fs.readFile(path, function (err, buffer) {
  const demo = new demofile.DemoFile()

  // overall
  let ticksPerSec = 64
  let playerIdleSince = {}
  let playerIdleAtPos = {}

  // collect round specific start info
  let roundNumber = 0
  let roundStartTick = 0

  let c = 0
  let f = 0

  demo.gameEvents.on('round_start', e => {
    playerIdleAtPos = {}
    playerIdleSince = {}
    ticksPerSec = demo.header.playbackTicks / demo.header.playbackTime
    roundNumber += 1
    roundStartTick = demo.currentTick
    console.log(`phase -> ${demo.gameRules.phase}`)
  })

  demo.entities.on('create', e => {
    if (e.entity.serverClass.name === 'CCSPlayer' && !e.entity.userInfo.fakePlayer && !e.entity.userInfo.isHltv) {
      console.log(`${e.entity.name} joined team ${e.entity.teamNumber}`);
      cache.addTeamData(e.entity.steamId, matchId)
    }
  })

  demo.stringTables.on('update', e => {
    if (e.table.name === 'userinfo' && e.userData != null) {
      // console.log('Player info updated:');
      console.log(e.entryIndex, e.userData);
    }
  })

  demo.gameEvents.on('event', e => {
    c += 1
  })

  demo.gameEvents.on('player_footstep', e => {
    // console.log(`death -> ${JSON.stringify(e)}`)
    if (roundNumber > 0) {
      f += 1
      const player = demo.entities.getByUserId(e.userid)
      const roundTime = (demo.currentTick - roundStartTick) / ticksPerSec
      // console.log(`footstep[${player.name}][${player.teamNumber}] pos -> ${JSON.stringify(translateCoordinates(demo.header.mapName, player.position.x, player.position.y))} (round ${roundNumber} time ${roundTime})`)

      // below code must execute if round changed or player died
      // otherwise the first footstep event of next round will be captured in negative
      const keyData = {
        matchId,
        steamId: player.steamId,
        roundNumber,
        team: player.teamNumber,
        phase: demo.gameRules.phase
      }

      const timeSpent = roundTime - playerIdleSince[e.userid]
      // console.log(`${player.name} spent ${timeSpent} seconds at ${playerIdleAtPos[e.userid]}`);
      cache.addHeatmapData('pos', playerIdleAtPos[e.userid], timeSpent, keyData)
      playerIdleSince[e.userid] = roundTime
      // playerIdleAtPos[e.userid] = JSON.stringify(player.position) // should we scale it?
      playerIdleAtPos[e.userid] = JSON.stringify(translateCoordinates(demo.header.mapName, player.position.x, player.position.y))
    }
  })

  demo.on('end', e => {
    console.log(`done parsing ${c} events`)
    console.log(`done parsing ${f} footsteps`)
    // collect all meta data
    console.log(`--------- ${demo.header.mapName} ${demo.header.serverName} - tickRate -> ${ticksPerSec}`);
    cache.addMatchData(matchId, demo.header)
  })

  demo.on('progress', e => {
    // console.log(`progress -> ${(e*100).toFixed(2)}%`)
  })

  demo.parse(buffer)
  // })
}

module.exports = { parse }
