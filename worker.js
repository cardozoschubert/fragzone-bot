require('dotenv').config()

const db = require('./db')

const kue = require('kue')

const demoManager = require('./demoManager')

const queue = kue.createQueue({
  redis: {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST
    // auth: 'password',
    // db: 3, // if provided select a non-default redis db
    // options: {
    //   // see https://github.com/mranney/node_redis#rediscreateclient
    // }
  }
})

const Bot = require('./bot')

queue.process('live', (job, done) => {
  Bot.checkForDemo(job.data.steamId, job.data.matchId, demoAvailable => {
    if (demoAvailable) {
      // db.updateStatsRequest(
      //   3281191646472962000, {
      //     UpdateExpression: 'set MATCH_STATUS = :s',
      //     ExpressionAttributeValues: {
      //       ':s': 'finished'
      //     }
      //   },
      //   () => {
      //     console.log('done')
      Bot.beFriend(job.data.steamId)
      //     // send a friend request
      //     // enqueue a job to remove friend after 1 hour
      //     done()
      //   }
      // )
    } else {
      done(new Error())
    }
  })
})

queue.process('parse', (job, done) => {
  db.findMatch(job.data.matchId, (err, matchData) => {
    console.log(`downloading ${JSON.stringify(matchData)}`);
    if (matchData && matchData.DEMO_URL) {
      demoManager.parseFromUrl(
        job.data.matchId,
        matchData.DEMO_URL,
        progress => {
          job.progress(progress, 100);
        },
        (err, buffer) => {
          console.log(`buffer length -> ${buffer.length}`);
          done(err)
        }
      )
    }
  })
})
