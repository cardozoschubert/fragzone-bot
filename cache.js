const redis = require('redis')

const client = redis.createClient()

const { promisify } = require('util')

const zrevrangeAsync = promisify(client.zrevrange).bind(client);

const smembersAsync = promisify(client.smembers).bind(client)

const addHeatmapData = (eventName, data, value, keyData) => {
  if (!data) return
  client.zincrby([`${eventName}:${keyData.steamId}:${keyData.matchId}:${keyData.team}:${keyData.phase}`, value, data], (err, response) => {
    console.log(`added to ${eventName} zset`)
  })
}

const addTeamData = (steamId, matchId) => {
  client.sadd([`team:${matchId}`, steamId], (err, response) => {
    console.log('added to set');
  })
}

const addMatchData = (matchId, data) => {
  client.hmset([
    `match:${matchId}`,
    'mapName',
    data.mapName,
    'clientName',
    data.clientName,
    'serverName',
    data.serverName
    // more stuff can be added
  ])
}

const pointsFromData = (data, scale) => (
  data.reduce((result, value, index, array) => {
    if (index % 2 === 0) {
      // result.push(array.slice(index, index + 2))
      const { x, y } = JSON.parse(array[index])
      // const y = JSON.parse(array[index]).y
      const value = parseInt(array[index + 1])
      result.push({ x, y, value })
    }
    return result;
  }, [])
)

// return response.map(e => client.zrevrange([`${eventType}:${e}:${matchId}:${team}:${phase}`, 0, -1, 'WITHSCORES'], (err, response) => {
//   return { [e]: pointsFromData(response) }
// }))
// }
// )


const getHeatmapData = async (matchId, eventType, team, phase, scale) => {
  const steamIds = await smembersAsync(['team:TEMP'])
  console.log(`obtained steamIds -> ${steamIds}`);
  const allData = await Promise.all(steamIds.map(async steamId => {
    const data = await zrevrangeAsync([`${eventType}:${steamId}:${matchId}:${team}:${phase}`, 0, -1, 'WITHSCORES'])
    console.log(`data for ${steamId} -> ${data.length}`)
    return data
  }))
  const results = {}
  steamIds.forEach((key, i) => {
    results[key] = pointsFromData(allData[i], scale)
  })
  return results
}

const createMatch = async (matchId, outcomeId, tokenId, demoUrl) => {
  client.hmset(`matches`)
}

module.exports = {
  addHeatmapData,
  addTeamData,
  addMatchData,
  getHeatmapData,
  createMatch
}
