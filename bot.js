const Steam = require('steam')

const steamClient = new Steam.SteamClient()
const steamUser = new Steam.SteamUser(steamClient)
const steamGC = new Steam.SteamGameCoordinator(steamClient, 730)
const steamFriends = new Steam.SteamFriends(steamClient)

const csgo = require('csgo')

const CSGO = new csgo.CSGOClient(steamUser, steamGC, false)

const db = require('./db')

const kue = require('kue')

const Long = require('long')

const queue = kue.createQueue({
  redis: {
    port: process.env.REDIS_PORT,
    host: process.env.REDIS_HOST
    // auth: 'password',
    // db: 3, // if provided select a non-default redis db
    // options: {
    //   // see https://github.com/mranney/node_redis#rediscreateclient
    // }
  }
})

const connectLoginAndLaunch = callback => {
  CSGO.once('ready', () => {
    callback()
  })

  steamClient.once('connected', () => {
    steamUser.logOn({ account_name: process.env.STEAM_USER, password: process.env.STEAM_PASS })
  })

  steamClient.once('logOnResponse', logonResp => {
    console.log(`logOnResponse -> ${logonResp.eresult}, steamID -> ${steamClient.steamID}`)
    steamFriends.setPersonaState(Steam.EPersonaState.Online)
    CSGO.launch()
  })

  steamClient.connect()
}

const decodeMatchCode = matchCode => {
  const scDecoder = new csgo.SharecodeDecoder(matchCode)
  console.log(`share code -> ${matchCode}`)
  // const { matchId, outcomeId, tokenId } =
  return scDecoder.decode()
}

// const isMatchInProgress = match => {
//   const lastRound = match.roundstatsall ? match.roundstatsall[match.roundstatsall.length - 1] : match.roundstats_legacy
//   return lastRound.map === null
// }

const getMatchId = match => {
  const matchIDobj = JSON.parse(JSON.stringify(match, null, 2))
  return new Long(matchIDobj.matchid.low, matchIDobj.matchid.high, matchIDobj.matchid.unsigned).toString()
}

const enqueueDemoParserJob = (matchId, callback) => {
  const job = queue.create('parse', { matchId }).save((err) => {
    if (!err) {
      console.log(`enqueueDemoParserJob -> ${job.id}`)
      // callback(null, job.id)
    } else {
      // callback(err)
    }
  })
}

const enqueueMatchPollerJob = (matchId, steamID) => {
  const job = queue.create('live', { matchId, steamID }).save((err) => {
    if (!err) console.log(`enqueueMatchPollerJob -> ${job.id}`)
  })
}

const requestMatch = (matchCode, callback) => {
  console.log('requestMatch');
  const { matchId, outcomeId, tokenId } = decodeMatchCode(matchCode)
  CSGO.once('matchList', list => {
    console.log('Match List ...')
    if (list.matches && list.matches.length > 0) {
      const match = list.matches[0]
      callback(null, match)
    } else {
      callback('Match not found', null)
    }
  })
  CSGO.requestGame(matchId, outcomeId, parseInt(tokenId))
}

const requestLiveMatch = (steamID, callback) => {
  CSGO.once('matchList', list => {
    if (list.matches && list.matches.length > 0) {
      const match = list.matches[0]
      callback(null, match)
    } else {
      callback('No Competitive Game', null)
    }
  })
  CSGO.requestLiveGameForUser(steamID)
}

const checkForDemo = (steamID, matchId, callback) => {
  requestLiveMatch(steamID, (err, match) => {
    if (err || (getMatchId(match) !== matchId)) {
      // no match or different match
      callback(true)
    } else {
      // db.update matchinprogress
      // fail job
      callback(false)
    }
  })
}

// -- DOC
// given a matchCode, gets the stats url
// consumed by http service, which redirects to that url
// consumed by the steam bot that sends the url as a message
const findOrRequestMatch = (matchCode, callback) => {
  const { matchId, outcomeId, tokenId } = decodeMatchCode(matchCode)

  db.findMatch(matchId, (err, matchData) => {
    if (matchData && matchData.STATUS === 'complete') {
      console.log('matchData exists');
      // this means that someone (anyone) already has created this match
      // the demo url will be there already, and STATUS is complete
    } else if (matchData === undefined || matchData.STATUS === undefined) {
      // match does not exist or is in STATUS = 'queued' or 'wip'
      console.log('matchData does not exist')
      requestMatch(matchCode, (err, match) => {
        const roundStats = match.roundstatsall
        const url = roundStats[roundStats.length - 1].map
        db.createMatch(
          `${matchId}`,
          {
            OUTCOME_ID: outcomeId, TOKEN_ID: tokenId, DEMO_URL: url, JOB_STATUS: 'queued'
          },
          () => {
            console.log(`created match with demo_url -> ${url} [QUEUED]`)
            // enqueue job
            enqueueDemoParserJob(matchId)
          }
        )
      })
    }
    const statsUrl = 'http://stats_url?token=sometoken'
    callback(null, statsUrl)
  })
}

steamFriends.on('friendMsg', (steamID, message, chatEntrytype) => {
  // check if messsage has a match share code, if it exists, reply that it exists, otherwise processing
  if (chatEntrytype === Steam.EChatEntryType.ChatMsg) {
    console.log(`friend message from ${steamID}: ${message} [${chatEntrytype}]`)
    if (message === '?') {
      // show help on copy share code
      steamFriends.sendMessage(steamID, 'Here\'s how...')
    } else {
      const result = message.match(/(CSGO-[a-zA-Z0-9]{5}-[a-zAA-Z0-9]{5}-[a-zAA-Z0-9]{5}-[a-zAA-Z0-9]{5}-[a-zAA-Z0-9]{5})/)
      if (result) {
        // found
        const matchCode = result[1]
        findOrRequestMatch(matchCode, (err, statsUrl) => {
          // the statsUrl can be sent to the requester via message, open on browser
          // if the status url is null, then you can tell the user, we will get it ...
          if (statsUrl) {
            steamFriends.sendMessage(steamID, `Your stats are ready -> ${statsUrl}`)
          } else {
            steamFriends.sendMessage(steamID, 'Hang tight...')
          }
        })
      } else {
        // i didn't understand
        steamFriends.sendMessage(steamID, 'What?')
      }
    }
  }
})

// a friend request was accepted, yipee.. but we're 50% there
steamFriends.on('friend', (steamID, relationship) => {
  if (relationship === Steam.EFriendRelationship.Friend) {
    console.log(`now friends with ${steamID}`)
    // check the db if there
    steamFriends.sendMessage(
      steamID,
      'Hello, To get the stats you requested, send me the match share code link (type \'?\' if you don\'t know how)'
    )
  }
})

const beFriend = (steamID) => {
  console.log('befriending');
  steamFriends.addFriend(steamID)
}

module.exports = {
  checkForDemo,
  connectLoginAndLaunch,
  decodeMatchCode,
  requestLiveMatch,
  getMatchId,
  beFriend,
  enqueueDemoParserJob,
  enqueueMatchPollerJob,
  findOrRequestMatch
}
