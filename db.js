const AWS = require('aws-sdk')

const dynamo = new AWS.DynamoDB({
  endpoint: process.env.AWS_DYNAMODB_ENDPOINT,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION
})

dynamo.listTables((err, data) => {
  console.log('listTables', err, data)
})

const docClient = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10', service: dynamo });
//
// const tickInfoParams = {
//   AttributeDefinitions: [
//     {
//       AttributeName: 'STEAM_ID_MATCH_ID',
//       AttributeType: 'S'
//     },
//     {
//       AttributeName: 'TICK',
//       AttributeType: 'N'
//     }
//   ],
//   KeySchema: [
//     {
//       AttributeName: 'STEAM_ID_MATCH_ID',
//       KeyType: 'HASH'
//     },
//     {
//       AttributeName: 'TICK',
//       KeyType: 'RANGE'
//     }
//   ],
//   ProvisionedThroughput: {
//     ReadCapacityUnits: 1,
//     WriteCapacityUnits: 1
//   },
//   TableName: 'TICK_INFO',
//   StreamSpecification: {
//     StreamEnabled: false
//   }
// }
//
// // Call DynamoDB to create the table
// dynamo.createTable(tickInfoParams, (err, data) => {
//   if (err) {
//     // console.log("Error", err)
//   } else {
//     console.log('Success', data)
//   }
// })

const statsRequestParams = {
  AttributeDefinitions: [
    {
      AttributeName: 'STEAM_ID',
      AttributeType: 'S'
    },
    {
      AttributeName: 'MATCH_ID',
      AttributeType: 'S'
    }
  ],
  KeySchema: [
    {
      AttributeName: 'STEAM_ID',
      KeyType: 'HASH'
    },
    {
      AttributeName: 'MATCH_ID',
      KeyType: 'RANGE'
    }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
  },
  TableName: 'STATS_REQUESTS',
  StreamSpecification: {
    StreamEnabled: false
  }
}

// Call DynamoDB to create the table
dynamo.createTable(statsRequestParams, (err, data) => {
  if (err) {
    console.log('Error', err)
  } else {
    console.log('Success', data)
  }
})

const matchParams = {
  AttributeDefinitions: [
    {
      AttributeName: 'MATCH_ID',
      AttributeType: 'S'
    }
  ],
  KeySchema: [
    {
      AttributeName: 'MATCH_ID',
      KeyType: 'HASH'
    },
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
  },
  TableName: 'MATCHES',
  StreamSpecification: {
    StreamEnabled: false
  }
}

// Call DynamoDB to create the table
dynamo.createTable(matchParams, (err, data) => {
  if (err) {
    console.log('Error', err)
  } else {
    console.log('Success', data)
  }
})


const tickInfoParams = {
  AttributeDefinitions: [
    {
      AttributeName: 'STEAM_ID',
      AttributeType: 'S'
    },
    {
      AttributeName: 'MATCH_ID',
      AttributeType: 'S'
    }
  ],
  KeySchema: [
    {
      AttributeName: 'STEAM_ID',
      KeyType: 'HASH'
    },
    {
      AttributeName: 'MATCH_ID',
      KeyType: 'RANGE'
    }
  ],
  ProvisionedThroughput: {
    ReadCapacityUnits: 1,
    WriteCapacityUnits: 1
  },
  TableName: 'STATS_REQUESTS',
  StreamSpecification: {
    StreamEnabled: false
  }
}

// Call DynamoDB to create the table
dynamo.createTable(statsRequestParams, (err, data) => {
  if (err) {
    console.log('Error', err)
  } else {
    console.log('Success', data)
  }
})


const createStatsRequest = (matchId, data, callback) => {
  const params = {
    TableName: 'STATS_REQUESTS',
    Item: {
      MATCH_ID: matchId,
      ...data
    }
  }

  docClient.put(params, (err, data) => {
    if (err) {
      console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
    } else {
      console.log('Added item:', JSON.stringify(data, null, 2));
    }
    callback()
  })
}

const createMatch = (matchId, data, callback) => {
  const params = {
    TableName: 'MATCHES',
    Item: {
      MATCH_ID: matchId,
      ...data
    }
  }

  docClient.put(params, (err, data) => {
    if (err) {
      console.error('Unable to add item. Error JSON:', JSON.stringify(err, null, 2));
    } else {
      console.log('Added item:', JSON.stringify(data, null, 2));
    }
    callback()
  })
}

const findMatch = (matchId, callback) => {
  const params = {
    TableName: 'MATCHES',
    Key: {
      MATCH_ID: matchId
    }
  };

  docClient.get(params, (err, data) => {
    if (err) {
      console.log(err)
      callback(err)
    }
    callback(null, data.Item)
  })
}

const updateMatch = (matchId, data, callback) => {
  const params = {
    TableName: 'MATCHES',
    Key: {
      MATCH_ID: matchId,
    },
    ...data
  }

  docClient.update(params, (err, data) => {
    if (err) {
      console.log('Error', err);
    } else {
      console.log('Success', data);
    }
    callback()
  })
}

module.exports = { createStatsRequest, createMatch, findMatch }
